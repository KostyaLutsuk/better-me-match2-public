﻿using RSG;
using UnityEngine;

namespace Game.Systems
{
    public class SaveSystem : MonoBehaviour
    {
        private const string PP_LEVEL_KEY = "level";

        private int _levelIndex;

        public IPromise<int> LoadLevelIndex()
        {
            var promise = new Promise<int>();
            _levelIndex = PlayerPrefs.HasKey(PP_LEVEL_KEY) ? PlayerPrefs.GetInt(PP_LEVEL_KEY) : 0;
#if UNITY_EDITOR
            Debug.Log($"Loaded level index = {_levelIndex}");
#endif
            promise.Resolve(_levelIndex);
            return promise;
        }

        public IPromise SaveLevelIndex()
        {
            var promise = new Promise();
            PlayerPrefs.SetInt(PP_LEVEL_KEY, _levelIndex);
            promise.Resolve();
            return promise;
        }

        public IPromise LevelPassed()
        {
            var promise = new Promise();
            _levelIndex++;
            promise.Resolve();
            return promise;
        }
    }
}
