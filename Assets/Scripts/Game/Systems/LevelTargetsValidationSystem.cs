﻿using System;
using System.Collections.Generic;
using System.Linq;
using Data;
using Match2;
using UnityEngine;

namespace Game.Systems
{
    public class LevelTargetsValidationSystem : MonoBehaviour, ILevelDataReceiver
    {
        public static event Action<BlockType, int> CollectedBlocksAmountChangedEvent;
    
        public event Action AllTargetsCompletedEvent;
    
        private readonly Dictionary<BlockType, int> _collectedBlocks = new Dictionary<BlockType, int>();

        private LevelTargetSet _levelTargetSet;

        private void Awake()
        {
            CellGrid.BlocksCollectedEvent += OnBlocksCollectedEvent;
        }

        private void OnDestroy()
        {
            CellGrid.BlocksCollectedEvent -= OnBlocksCollectedEvent;
        }

        private void OnBlocksCollectedEvent(BlockType blockType, int count)
        {
            AddCollectedBlocks(blockType, count);
        
            if (AllTargetsCompleted())
            {
                AllTargetsCompletedEvent?.Invoke();
            }
        }

        private void AddCollectedBlocks(BlockType type, int amount)
        {
            if (_collectedBlocks.ContainsKey(type))
                _collectedBlocks[type] += amount;
            else
                _collectedBlocks.Add(type, amount);

            CollectedBlocksAmountChangedEvent?.Invoke(type, _collectedBlocks[type]);
        }

        private bool AllTargetsCompleted()
        {
            return _levelTargetSet.Targets.All(TargetCompleted);
        }

        private bool TargetCompleted(LevelTarget levelTarget)
        {
            var type = levelTarget.Type;
            return _collectedBlocks.ContainsKey(type) && _collectedBlocks[type] >= levelTarget.Amount;
        }

        private void Clear()
        {
            _collectedBlocks.Clear();
        }

        public void PassLevelData(LevelData levelData)
        {
            Clear();
            _levelTargetSet = levelData.LevelTargetSet;
        }
    }

    public class BlockTypeCounter
    {
        private BlockType _blockType;
        private int _amount;
    
    }
}