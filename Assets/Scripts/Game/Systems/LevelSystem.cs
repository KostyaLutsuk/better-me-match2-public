﻿using Data;
using RSG;
using UnityEngine;

namespace Game.Systems
{
    public class LevelSystem : MonoBehaviour
    {
        [SerializeField] private GameData _gameData;

        public IPromise<LevelData> LoadLevel(int idx)
        {
            var promise = new Promise<LevelData>();
            var levelData = _gameData.LevelDatas[Mathf.Clamp(idx, 0, _gameData.LevelDatas.Count - 1)];
            promise.Resolve(levelData);
        
            return promise;
        }
    }
}
