﻿using System.Collections.Generic;
using Data;
using Game.Systems;
using Match2;
using RSG;
using UnityEngine;

namespace Game
{
    [RequireComponent(typeof(SaveSystem), typeof(LevelSystem))]
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private List<MonoBehaviour> _levelDataReceivers;

        [SerializeField] private SaveSystem _saveSystem;
        [SerializeField] private LevelSystem _levelSystem;
        [SerializeField] private LevelTargetsValidationSystem _targetsValidationSystem;
        [SerializeField] private CellGrid _cellGrid;

        public void ActivateGame()
        {
            _targetsValidationSystem.AllTargetsCompletedEvent += OnAllTargetsCompleted;
        
            _saveSystem.LoadLevelIndex()
                .Then(_levelSystem.LoadLevel)
                .Then(PassLevelData)
                .Then(() => _cellGrid.Create())
                .Then(() => _cellGrid.FillEmpty(true))
                .Catch(ex => Debug.LogError(ex));
        }

        public void DeactivateGame()
        {
            _targetsValidationSystem.AllTargetsCompletedEvent -= OnAllTargetsCompleted;
        
            _saveSystem.SaveLevelIndex()
                .Then(() => _cellGrid.Clear());
        }

        // HACK: smth similar to DI but faster to implement
        private void PassLevelData(LevelData levelData)
        {
            foreach (var behaviour in _levelDataReceivers)
            {
                if (behaviour is ILevelDataReceiver levelDataReceiver)
                {
                    levelDataReceiver.PassLevelData(levelData);
                }
            }
        }

        private void OnAllTargetsCompleted()
        {
            GoToNextLevel();
        }

        private IPromise GoToNextLevel()
        {
            return Promise.Resolved()
                .Then(DeactivateGame)
                .Then(_saveSystem.LevelPassed)
                .Then(_saveSystem.SaveLevelIndex)
                .Then(ActivateGame);
        }

        private void OnValidate()
        {
            _saveSystem = GetComponent<SaveSystem>();
            _levelSystem = GetComponent<LevelSystem>();
            _targetsValidationSystem = GetComponent<LevelTargetsValidationSystem>();
        }
    }

    internal interface ILevelDataReceiver
    {
        void PassLevelData(LevelData levelData);
    }
}