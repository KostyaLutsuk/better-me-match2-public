﻿using UnityEngine;

namespace UI
{
    public abstract class BaseUI : MonoBehaviour, IActivatable
    {
        [SerializeField] protected GameObject Root;

        private void OnValidate()
        {
            if (Root == null)
            {
                if (transform.childCount == 0)
                    return;
            
                var child = transform.GetChild(0);
            
                Root = child.gameObject;
            }
        }

        public void Activate()
        {
            Root.SetActive(true);
        }

        public void Deactivate()
        {
            Root.SetActive(false);
        }
    }
}
