﻿using ScriptableObjectArchitecture;
using UnityEngine;

namespace UI.MainMenu
{
    public class MainMenuUI : BaseUI
    {
        [SerializeField] private GameEventBase _onPlayBtnEvent;
        [SerializeField] private GameEventBase _onSettingsBtnEvent;


        public void OnPlayBtn()
        {
            _onPlayBtnEvent.Raise();
        }
    
        public void OnSettingsBtn()
        {
            _onSettingsBtnEvent.Raise();
        }
    }
}
