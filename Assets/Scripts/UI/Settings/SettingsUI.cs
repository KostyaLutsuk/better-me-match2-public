﻿using ScriptableObjectArchitecture;
using UnityEngine;

namespace UI.Settings
{
    public class SettingsUI : BaseUI
    {
        [SerializeField] private GameEventBase _onBackBtnEvent;

        public void OnBackBtn()
        {
            _onBackBtnEvent.Raise();
        }
    }
}
