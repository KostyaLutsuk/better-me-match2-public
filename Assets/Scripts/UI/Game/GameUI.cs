﻿using ScriptableObjectArchitecture;
using UnityEngine;

namespace UI.Game
{
    public class GameUI : BaseUI
    {
        [SerializeField] private GameEventBase _onBackBtnEvent;

        public void OnBackBtn()
        {
            _onBackBtnEvent.Raise();
        }
    }
}
