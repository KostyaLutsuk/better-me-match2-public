﻿using System.Collections.Generic;
using Data;
using Game;
using UnityEngine;

namespace UI.Game
{
    public class LevelTargetsUI : MonoBehaviour, ILevelDataReceiver
    {
        [SerializeField] private LevelTargetsItemUI _itemOrigin;
        [SerializeField] private RectTransform _content;

        private LevelData _levelData;
        private List<LevelTargetsItemUI> _items = new List<LevelTargetsItemUI>();

        public void PassLevelData(LevelData levelData)
        {
            ClearTargets();
        
            _levelData = levelData;
            var targetSet = levelData.LevelTargetSet;
            foreach (var target in targetSet.Targets)
            {
                var item = Instantiate(_itemOrigin, _content);
                item.Init(target);
                _items.Add(item);
                item.gameObject.SetActive(true);
            }
        }

        private void ClearTargets()
        {
            if (_items.Count == 0)
                return;
        
            foreach (var item in _items)
            {
                Destroy(item.gameObject); // TODO: implement object pooling
            }
            _items.Clear();
        }
    }
}
