﻿using Data;
using Game.Systems;
using Match2;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Game
{
    public class LevelTargetsItemUI : MonoBehaviour
    {
        [SerializeField] private Image _typeImage;
        [SerializeField] private TextMeshProUGUI _amountText;

        private LevelTarget _levelTarget;

        private void Awake()
        {
            LevelTargetsValidationSystem.CollectedBlocksAmountChangedEvent += OnCollectedBlocksAmountChangedEvent;
        }

        private void OnDestroy()
        {
            LevelTargetsValidationSystem.CollectedBlocksAmountChangedEvent -= OnCollectedBlocksAmountChangedEvent;
        }

        private void OnCollectedBlocksAmountChangedEvent(BlockType blockType, int amount)
        {
            if (blockType != _levelTarget.Type)
                return;

            UpdateAmount(Mathf.Max(0, _levelTarget.Amount - amount));
        }

        private void UpdateAmount(int newAmount)
        {
            _amountText.text = newAmount.ToString();
        }

        public void Init(LevelTarget levelTarget)
        {
            _levelTarget = levelTarget;
            _typeImage.sprite = levelTarget.Type.PreviewSprite;
            UpdateAmount(levelTarget.Amount);
        }
    }
}
