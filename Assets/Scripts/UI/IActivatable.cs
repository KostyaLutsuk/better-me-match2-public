﻿namespace UI
{
    public interface IActivatable
    {
        void Activate();
        void Deactivate();
    }
}
