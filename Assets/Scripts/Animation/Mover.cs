﻿using Animation.Config;
using DG.Tweening;
using RSG;
using UnityEngine;

namespace Animation
{
    public class Mover : MonoBehaviour
    {
        [SerializeField] private MoverConfig _config;
    
        [SerializeField, HideInInspector]
        private RectTransform _rectTransform;
    
        private Tween _tween;
    
        public IPromise Move(Vector2 targetAnchoredPosition)
        {
            // Debug.Log($"Move from {_rectTransform.anchoredPosition} to {targetAnchoredPosition}");
            var promise = new Promise();
            _tween = _rectTransform.DOAnchorPos(targetAnchoredPosition, _config.RandomSpeed)
                .SetSpeedBased(true)
                .SetEase(_config.Ease);
            _tween.OnComplete(() => promise.Resolve());
            return promise;
        }

        public IPromise MoveImmediately(Vector2 targetAnchoredPosition)
        {
            var promise = new Promise();
            _rectTransform.anchoredPosition = targetAnchoredPosition;
            promise.Resolve();
            return promise;
        }

        public IPromise Stop()
        {
            var promise = new Promise();
            _tween?.Kill();
            promise.Resolve();
            return promise;
        }

        private void OnValidate()
        {
            _rectTransform = GetComponent<RectTransform>();
        }
    }
}
