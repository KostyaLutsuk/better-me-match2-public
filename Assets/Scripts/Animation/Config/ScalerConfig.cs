﻿using DG.Tweening;
using UnityEngine;

namespace Animation.Config
{
    [CreateAssetMenu]
    public class ScalerConfig : ScriptableObject
    {
        public float Speed = 5f;
        public Ease Ease = Ease.Linear;
    }
}
