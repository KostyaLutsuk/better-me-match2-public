﻿using DG.Tweening;
using UnityEngine;

namespace Animation.Config
{
    [CreateAssetMenu]
    public class MoverConfig : ScriptableObject
    {
        public float Speed = 10f;
        [Range(0, 1)]
        public float SpeedRandomFactor = 0.05f;
        public Ease Ease = Ease.OutBounce;

        public float RandomSpeed => Speed * (1 + SpeedRandomFactor * .5f - Random.value * SpeedRandomFactor);
    }
}
