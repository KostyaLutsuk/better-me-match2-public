﻿using Animation.Config;
using DG.Tweening;
using RSG;
using UnityEngine;

namespace Animation
{
    public class Scaler : MonoBehaviour
    {
        [SerializeField] private ScalerConfig _config;
    
        [SerializeField, HideInInspector]
        private RectTransform _rectTransform;

        private Tween _tween;

        public IPromise Scale(Vector3 targetScale)
        {
            var promise = new Promise();
            _tween = _rectTransform.DOScale(targetScale, _config.Speed)
                .SetSpeedBased(true)
                .SetEase(_config.Ease);
            _tween.OnComplete(() => promise.Resolve());
            return promise;
        }

        public IPromise Stop()
        {
            var promise = new Promise();
            _tween?.Kill();
            promise.Resolve();
            return promise;
        }

        private void OnValidate()
        {
            _rectTransform = GetComponent<RectTransform>();
        }
    }
}
