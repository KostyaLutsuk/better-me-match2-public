﻿using UnityEngine;

// [System.Serializable]
namespace Match2
{
    public class Cell
    {
        public Vector2Int Index { get; }

        public Vector2 AnchoredPosition { get; }
    
        public Block Block { get; private set; }

        public bool IsEmpty => Block == null;

        public Cell(Vector2Int index, Vector2 anchoredPosition)
        {
            Index = index;
            AnchoredPosition = anchoredPosition;
        }

        public void Fill(Block block)
        {
            if (!IsEmpty)
            {
                // Debug.LogError($"Cell [{Index}] is not empty.");
                return;
            }

            Block = block;
            block.Index = Index;
            // block.Cell = this;
        }

        public Block Clear()
        {
            var block = Block;
            Block = null;
            return block;
        }
    }
}
