﻿using UnityEngine;

namespace Match2
{
    /// <summary>
    /// Like a enum but could be changed by gamedesigners cause it is not hardcoded.
    /// </summary>
    [CreateAssetMenu]
    public class BlockType : ScriptableObject
    {
        public Sprite PreviewSprite;
    }
}
