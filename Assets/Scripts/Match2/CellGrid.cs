﻿using System.Collections.Generic;
using System.Linq;
using Data;
using Game;
using Match2.Config;
using RSG;
using UnityEngine;

namespace Match2
{
    [RequireComponent(typeof(BlockSpawner))]
    public class CellGrid : MonoBehaviour, ILevelDataReceiver
    {
        private const int MIN_LINKED_BLOCKS_AMOUNT = 2;

        public delegate void BlocksCollectedEventHandler(BlockType blockType, int count);
        public static event BlocksCollectedEventHandler BlocksCollectedEvent;

        [SerializeField] private CellGridConfig _config;
        [SerializeField] private BlockSpawner _blockSpawner;
        [SerializeField] private RectTransform _cellParent;
    
        private Cell[,] _cells;
        private int _width;
        private int _height;
        private LevelData _levelData;

        private void Create(int width, int height)
        {
            _cells = new Cell[width, height];
            _width = width;
            _height = height;
            var halfAnchWidth = _width * _config.SpaceBetweenCells * .5f;
            var halfAnchHeight = _height * _config.SpaceBetweenCells * .5f;
            var halfSpace = _config.SpaceBetweenCells * .5f;
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Vector2 anchPos = new Vector2(i * _config.SpaceBetweenCells - halfAnchWidth + halfSpace,
                        j * _config.SpaceBetweenCells - halfAnchHeight + halfSpace);
                    _cells[i, j] = new Cell(new Vector2Int(i, j), anchPos);
                }
            }
        }

        public void Create()
        {
            Create(_levelData.Width, _levelData.Height);
        }

        public void Clear()
        {
            foreach (var cell in _cells)
            {
                ClearCell(cell, true);
            }

            _isBusy = false;
        }

        public IPromise FillEmpty(bool immediately = false)
        {
            var emptyCells = new List<Cell>();
            for (int i = 0; i < _width; i++)
            {
                for (int j = 0; j < _height; j++)
                {
                    var cell = _cells[i, j];
                    if (cell.IsEmpty)
                    {
                        emptyCells.Add(cell);
                    }
                }
            }

            return Promise.All(emptyCells.Select(c => FillCell(c, immediately))).Catch(ex => Debug.LogError(ex.Message));
        }

        private IPromise FillCell(Cell cell, bool immediately = false)
        {
            // Debug.Log($"FillCell({cell.Index})");
            var block = _blockSpawner.Spawn(cell.AnchoredPosition, _cellParent);
            block.transform.SetParent(_cellParent);
            block.Clicked += OnBlockClicked;

            if (immediately)
            {
                return MoveBlockImmediately(block, cell, false);
            }

            return MoveBlockImmediately(block, cell, true)
                .Then(() => MoveBlock(block, cell));
        }

        private IPromise MoveBlockImmediately(Block block, Cell targetCell, bool withOffset)
        {
            var mover = block.Mover;
            targetCell.Fill(block);
            return mover.MoveImmediately(targetCell.AnchoredPosition +
                                         (withOffset ? _config.SpawnOffsetFromCell : Vector2.zero));
        }

        private IPromise MoveBlock(Cell sourceCell, Cell targetCell)
        {
            var block = sourceCell.Clear();
            return MoveBlock(block, targetCell);
        }

        private IPromise MoveBlock(Block block, Cell targetCell)
        {
            var mover = block.Mover;
            targetCell.Fill(block);
            return mover.Move(targetCell.AnchoredPosition);
        }

        private IPromise Fall()
        {
            List<IPromise> promises = new List<IPromise>();
            for (int i = 0; i < _width; i++)
            {
                for (int j = 1; j < _height; j++)
                {
                    var cell = _cells[i, j];
                    var block = cell.Block;
                    if (block == null)
                        continue;
                    var fallingTarget = GetFallingTarget(cell);
                    if (fallingTarget != cell)
                    {
                        promises.Add(MoveBlock(cell, fallingTarget));
                    }
                }
            }

            return Promise.All(promises);
        }

        private Cell GetFallingTarget(Cell cell)
        {
            Cell fallingTarget = cell;
            while (true)
            {
                Cell neighborCell = GetNeighborFor(fallingTarget, Sides.Bottom);
                if (neighborCell == null)
                {
                    break;
                }

                if (neighborCell.Block == null)
                {
                    fallingTarget = neighborCell;
                }
                else
                {
                    break;
                }
            }

            return fallingTarget;
        }

        private bool _isBusy;

        private void OnBlockClicked(Block block)
        {
            if (_isBusy)
                return;

            // Debug.Log($"Block [{block.Index}] clicked");
            _isBusy = true;
            // find nearest cells of the same color
            var idx = block.Index;
            var type = block.Type;
            Cell clickedCell = _cells[idx.x, idx.y];
            var sameTypeCells = GetLinkedCellsOfSameType(clickedCell);
            var count = sameTypeCells.Count;

            if (count >= MIN_LINKED_BLOCKS_AMOUNT)
            {
                var promise = Promise.All(
                    Promise.All(sameTypeCells.Select(c => ClearCell(c))),
                    Fall(),
                    FillEmpty()
                );

                promise.Done(() => _isBusy = false);
                BlocksCollectedEvent?.Invoke(type, count);
            }
            else
            {
                _isBusy = false;
            }
        }

        private IPromise ClearCell(Cell cell, bool immediately = false)
        {
            var block = cell.Clear();
            var scaler = block.Scaler;
            if (immediately)
            {
                var mover = block.Mover;
                return
                    mover.Stop()
                        .Then(() => scaler.Stop())
                        .Then(() => _blockSpawner.Despawn(block));
            }

            return
                scaler.Scale(Vector3.zero)
                    .Then(() => _blockSpawner.Despawn(block));
        }

        private Cell GetNeighborFor(Cell cell, Vector2Int offset)
        {
            Vector2Int neighborIndex = cell.Index;
            neighborIndex += offset;
            if (!IndexIsInsideCubesArray(neighborIndex))
            {
                return null;
            }

            return _cells[neighborIndex.x, neighborIndex.y];
        }

        private List<Cell> GetNeighborCellsOfSameType(Cell cell)
        {
            List<Cell> res = new List<Cell>();
            Block block = cell.Block;
            if (block == null)
                return res;
            BlockType type = block.Type;
            foreach (Vector2Int side in Sides.All)
            {
                Cell neighborCell = GetNeighborFor(cell, side);
                if (neighborCell == null)
                    continue;
                Block neighborBlock = neighborCell.Block;
                if (neighborBlock == null)
                    continue;
                if (type == neighborBlock.Type)
                    res.Add(neighborCell);
            }

            return res;
        }

        private List<Cell> GetLinkedCellsOfSameType(Cell cell)
        {
            List<Cell> res = new List<Cell> {cell};
            Block cube = cell.Block;
            if (cube == null)
                return res;
            List<Cell> checkedCells = new List<Cell> {cell};
            while (checkedCells.Count > 0)
            {
                Cell checkedCell = checkedCells[0];
                List<Cell> sameTypeNeighbors = GetNeighborCellsOfSameType(checkedCell);
                checkedCells.RemoveAt(0);
                List<Cell> newRes = sameTypeNeighbors.Except(res).ToList();
                checkedCells.AddRange(newRes);
                res.AddRange(newRes);
            }

            return res;
        }

        private bool IndexIsInsideCubesArray(Vector2Int index)
        {
            return index.x >= 0 && index.x < _width && index.y >= 0 && index.y < _height;
        }

        public void PassLevelData(LevelData levelData)
        {
            _levelData = levelData;
        }

        private void OnValidate()
        {
            _blockSpawner = GetComponent<BlockSpawner>();
        }
    }
}
