﻿using Data;
using Game;
using RSG;
using UnityEngine;

namespace Match2
{
    public class BlockSpawner : MonoBehaviour, ILevelDataReceiver
    {
        private LevelData _levelData;

        public Block Spawn(Vector2 targetPosition, RectTransform parent)
        {
            var prefab = _levelData.GetRandomBlockPrefab();
            var block = Instantiate(prefab, parent); // TODO: Implement object pooling
            return block;
        }

        public IPromise Despawn(Block block)
        {
            var promise = new Promise();
            Destroy(block.gameObject); // TODO: Implement object pooling
            promise.Resolve();
            return promise;
        }

        public void PassLevelData(LevelData levelData)
        {
            _levelData = levelData;
        }
    }
}
