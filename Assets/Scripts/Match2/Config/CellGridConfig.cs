﻿using UnityEngine;

namespace Match2.Config
{
    [CreateAssetMenu]
    public class CellGridConfig : ScriptableObject
    {
        public Vector2 SpawnOffsetFromCell = new Vector2(0, 1500);
        public float SpaceBetweenCells = 120f;
    }
}
