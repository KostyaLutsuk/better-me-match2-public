﻿using System.Collections;
using UnityEngine;

namespace Match2
{
    public static class Sides
    {
        public static readonly Vector2Int Top = new Vector2Int(0, 1);
        public static readonly Vector2Int Bottom = new Vector2Int(0, -1);
        public static readonly Vector2Int Right = new Vector2Int(1, 0);
        public static readonly Vector2Int Left = new Vector2Int(-1, 0);

        public static readonly Vector2IntEnumerable All = new Vector2IntEnumerable(new[]
        {
            Top,
            Bottom,
            Left,
            Right
        });

        public class Vector2IntEnumerable
        {
            private readonly Vector2Int[] _sides;

            public Vector2IntEnumerable(Vector2Int[] sides)
            {
                _sides = sides;
            }

            public IEnumerator GetEnumerator()
            {
                return _sides.GetEnumerator();
            }
        }
    }
}
