﻿using System;
using Animation;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Match2
{
    public class Block : MonoBehaviour, IPointerDownHandler
    {
        public BlockType Type;
    
        public event Action<Block> Clicked;

        public Vector2Int Index;

        // public Cell Cell;

        [SerializeField, HideInInspector]
        private RectTransform _rectTransform;

        [SerializeField, HideInInspector] private Mover _mover;
        [SerializeField, HideInInspector] private Scaler _scaler;
    
        public Mover Mover => _mover;
        public Scaler Scaler => _scaler;

        private void OnValidate()
        {
            _rectTransform = GetComponent<RectTransform>();
            _mover = GetComponent<Mover>();
            _scaler = GetComponent<Scaler>();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            Clicked?.Invoke(this);
        }
    }
}
