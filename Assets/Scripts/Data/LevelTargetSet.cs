﻿using System.Collections.Generic;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu]
    public class LevelTargetSet : ScriptableObject
    {
        public List<LevelTarget> Targets;
    }
}
