﻿using System.Collections.Generic;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu]    
    public class GameData : ScriptableObject
    {
        public List<LevelData> LevelDatas;
        public void ResetData()
        {
        
        }
    }
}
