﻿using Match2;

namespace Data
{
    [System.Serializable]
    public class LevelTarget
    {
        public BlockType Type;
        public int Amount;
    }
}
