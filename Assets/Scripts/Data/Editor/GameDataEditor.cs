﻿using UnityEditor;
using UnityEngine;

namespace Data.Editor
{
    [CustomEditor(typeof(GameData))]
    public class GameDataEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            GameData e = target as GameData;
            if (GUILayout.Button("Reset Data"))
            {
                e.ResetData();
            }
        }
    }
}