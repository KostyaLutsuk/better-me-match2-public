﻿using System.Collections.Generic;
using System.Linq;
using Match2;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu]
    public class LevelData : ScriptableObject
    {
        public int Height;
        public int Width;
        public LevelTargetSet LevelTargetSet;
        public List<BlockChance> BlockChances;

        public Block GetRandomBlockPrefab()
        {
            var sum = BlockChances.Sum(bc => bc.Weight);
            var rand = Random.Range(0, sum);
            sum = 0;
            foreach (var bc in BlockChances)
            {
                sum += bc.Weight;
                if (rand <= sum) return bc.Object;
            }

            return null;
        }

        public void ResetData()
        {

        }
    }

    [System.Serializable]
    public class ObjectChance<T>
    {
        public T Object;
        public float Weight;
    }


    [System.Serializable]
    public class BlockChance : ObjectChance<Block>{}
}