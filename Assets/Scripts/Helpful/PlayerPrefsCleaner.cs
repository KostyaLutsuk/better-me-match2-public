﻿using UnityEngine;

namespace Helpful
{
    public class PlayerPrefsCleaner : MonoBehaviour
    {
        public void Clear()
        {
            PlayerPrefs.DeleteAll();
            PlayerPrefs.Save();
        }
    }
}
