﻿using UnityEngine;

namespace Helpful
{
    public class TargetFramerateSetter : MonoBehaviour
    {
        [SerializeField] private int _framerate = 60;
        private void Awake()
        {
            Application.targetFrameRate = _framerate;
        }
    }
}
